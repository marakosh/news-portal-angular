import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { articles } from './../news-mock-data';

@Component({
  selector: 'app-article-detail-page',
  templateUrl: './article-detail-page.component.html',
  styleUrls: ['./article-detail-page.component.css']
})
export class ArticleDetailPageComponent implements OnInit {

  article;

  constructor(
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.setArticle();
  }

  setArticle(){
    const id = Number.parseInt(this.route.snapshot.paramMap.get('id'));
    const requiredArticle = articles.filter(article => {
      return article.uniqueId == id;
    })[0];
    console.log('[verbose] [marakosh] test log', requiredArticle);
    this.article = requiredArticle;
  }

}
