import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainPageComponent } from './main-page/main-page.component';
import { SearchPageComponent } from './search-page/search-page.component';
import { ArticleDetailPageComponent} from './article-detail-page/article-detail-page.component';


const routes: Routes = [
  { path: '', component: MainPageComponent},
  { path: 'search', component: SearchPageComponent},
  { path: 'main', redirectTo: ''},
  { path: 'article/:id', component: ArticleDetailPageComponent},
  { path: '**', redirectTo: ''}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
