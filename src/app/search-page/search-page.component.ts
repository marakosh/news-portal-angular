import { Component, OnInit } from '@angular/core';
import { articles } from './../news-mock-data';

@Component({
  selector: 'app-search-page',
  templateUrl: './search-page.component.html',
  styleUrls: ['./search-page.component.css']
})
export class SearchPageComponent implements OnInit {

  articles = [];

  constructor() { }

  ngOnInit(): void {
    this.articles = articles;
  }

}
