import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './shared/header/header.component';
import { SearchInputComponent } from './shared/search-input/search-input.component';
import { SearchButtonComponent } from './shared/search-button/search-button.component';
import { LatestNewsComponent } from './main-page/latest-news/latest-news.component';
import { TopNewsComponent } from './main-page/top-news/top-news.component';
import { MainPageComponent } from './main-page/main-page.component';
import { ArticleDetailPageComponent } from './article-detail-page/article-detail-page.component';
import { SearchPageComponent } from './search-page/search-page.component';
import { SearchResultItemComponent } from './search-page/search-result-item/search-result-item.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    SearchInputComponent,
    SearchButtonComponent,
    LatestNewsComponent,
    TopNewsComponent,
    MainPageComponent,
    ArticleDetailPageComponent,
    SearchPageComponent,
    SearchResultItemComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
