import { Component, OnInit } from '@angular/core';
import {articles} from '../../news-mock-data';

@Component({
  selector: 'app-top-news',
  templateUrl: './top-news.component.html',
  styleUrls: ['./top-news.component.css']
})
export class TopNewsComponent implements OnInit {

  //TODO: need to rewrite
  topNewsArticles = [articles[4]];

  constructor() { }

  ngOnInit(): void {
  }

}
