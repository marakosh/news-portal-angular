import { Component, OnInit } from '@angular/core';
import { articles } from '../../news-mock-data';

@Component({
  selector: 'app-latest-news',
  templateUrl: './latest-news.component.html',
  styleUrls: ['./latest-news.component.css']
})

export class LatestNewsComponent implements OnInit {

  articles = [];

  constructor() { }

  ngOnInit(): void {
    this.articles = articles.slice(articles.length-4, articles.length);
  }

}
