export const articles = [
    {
        "uniqueId": 1,
        "source": {
            "id": null,
            "name": "Buzzfeednews.com"
        },
        "author": "Jane Lytvynenko",
        "title": "Two Hairstylists With Coronavirus May Have Exposed More Than 140 Other People - BuzzFeed News",
        "description": "Businesses are reopening in Missouri, but a local leader warned that’s only safe if contact tracers can keep up with potential exposures.",
        "url": "https://www.buzzfeednews.com/article/janelytvynenko/missouri-hair-salon-coronavirus-exposure",
        "urlToImage": "https://img.buzzfeed.com/buzzfeed-static/static/2020-05/24/17/enhanced/28dde6190890/original-6791-1590339743-8.jpg?crop=1244:651;1,4",
        "publishedAt": "2020-05-24T18:25:31Z",
        "content": "Two hairstylists in Springfield, Missouri, have tested positive for COVID-19 and possibly exposed more than 140 clients, underscoring the difficulty local health departments will face in tracing the coronavirus as businesses reopen. \n The Springfield-Greene County Health Department announced the potential exposures\" in press conferences Friday and Saturday, adding that their team of seven contact tracers is in the process of getting in touch with anyone who may have been affected. They will undergo an interview with an immunologist and will be asked to monitor for symptoms of COVID-19."
    },
    {
        "uniqueId": 2,
        "source": {
            "id": "cnn",
            "name": "CNN"
         },
        "author": "Chloe Melas, CNN",
        "title": "Kirby Jenner, the 'fraternal twin' of Kendall Jenner, steps into the spotlight on Quibi - CNN",
        "description": "There's a new reality show from the Kardashian/Jenner family, but it has a twist.",
        "url": "https://www.cnn.com/2020/05/24/entertainment/kirby-jenner-interview-quibi-show/index.html",
        "urlToImage": "https://cdn.cnn.com/cnnnext/dam/assets/200522115953-kirby-jenner-2019-file-restricted-super-tease.jpg",
        "publishedAt": "2020-05-24T18:03:54Z",
        "content": "(CNN)There's a new reality show from the Kardashian/Jenner family, but it has a twist. \r\nThe series, titled \"Kirby Jenner,\" premieres Monday on Quibi. The show centers around a 24-year-old performanc… [+3608 chars]"
    },
    {
        "uniqueId": 3,
        "source": {
            "id": "cnn",
            "name": "CNN"
        },
        "author": "Peter Valdes-Dapena, CNN Business",
        "title": "Want to buy a used car? Rental car companies are offering up some good deals - CNN",
        "description": "With travelers staying close to home and airports turned into virtual ghost towns, rental car companies have taken a huge hit during the coronavirus pandemic. On Friday Hertz, which also operates the Dollar and Thrifty agencies, declared bankruptcy. Industry …",
        "url": "https://www.cnn.com/2020/05/24/business/buying-used-rental-cars/index.html",
        "urlToImage": "https://cdn.cnn.com/cnnnext/dam/assets/200522152148-avis-rental-full-parking-lot-restricted-super-tease.jpg",
        "publishedAt": "2020-05-24T17:55:02Z",
        "content": "New York (CNN)With travelers staying close to home and airports turned into virtual ghost towns, rental car companies have taken a huge hit during the coronavirus pandemic. On Friday Hertz, which als… [+4047 chars]"
    },
    {
        "uniqueId": 4,
        "source": {
            "id": "cnn",
            "name": "CNN"
        },
        "author": "Arman Azad, Nicky Robertson and Devan Cole, CNN",
        "title": "Here's what Dr. Deborah Birx had to say about Trump wearing a mask - CNN",
        "description": "White House coronavirus response coordinator Dr. Deborah Birx said Sunday that there is \"clear scientific evidence\" that masks work -- adding that she assumes President Donald Trump is able to keep six feet of distance \"in a majority of cases\" when pressed on…",
        "url": "https://www.cnn.com/2020/05/24/politics/deborah-birx-donald-trump-masks-social-distancing/index.html",
        "urlToImage": "https://cdn.cnn.com/cnnnext/dam/assets/200524120611-01-deborah-birx-mask-0522-super-tease.jpg",
        "publishedAt": "2020-05-24T17:42:53Z",
        "content": null
    },
    {
        "uniqueId": 5,
        "source": {
            "id": null,
            "name": "Theguardian.comscience"
        },
        "author": "Rupert Neate",
        "title": "Elon Musk’s SpaceX to launch first astronauts from US soil since 2011 - The Guardian",
        "description": "Falcon 9 rocket to make history as billionaire seeks to commercialise space travel",
        "url": "https://amp.theguardian.comscience/2020/may/24/elon-musks-spacex-to-launch-first-astronauts-from-us-soil-since-2011",
        "urlToImage": "https://i.guim.co.uk/img/media/9c6f0007fb6424d95ab2d35176562c6d470115b1/322_0_6532_3920/master/6532.jpg?width=1200&height=630&quality=85&auto=format&fit=crop&overlay-align=bottom%2Cleft&overlay-width=100p&overlay-base64=L2ltZy9zdGF0aWMvb3ZlcmxheXMvdGctZGVmYXVsdC5wbmc&enable=upscale&s=444c93a81d4aa35ecf3901177c036f12",
        "publishedAt": "2020-05-24T17:28:37Z",
        "content": "Elon Musks SpaceX company hopes to make history on Wednesday by launching the first astronauts into space from US soil in nine years, as the billionaire takes the next step in his dream to commercial… [+1205 chars]"
    },
    {
        "uniqueId": 6,
        "source": {
            "id": "cnn",
            "name": "CNN"
        },
        "author": "Eric Levenson and Amanda Jackson, CNN",
        "title": "Pool party at Lake of the Ozarks, Missouri draws a packed crowd - CNN",
        "description": "Video posted by a reporter shows partiers crowded together in a pool at the Lake of the Ozarks, Missouri this Memorial Day weekend.",
        "url": "https://www.cnn.com/2020/05/24/us/ozarks-missouri-party/index.html",
        "urlToImage": "https://cdn.cnn.com/cnnnext/dam/assets/200524110917-ozarks-memorial-day-pool-party-super-tease.jpg",
        "publishedAt": "2020-05-24T17:14:53Z",
        "content": "(CNN)Video posted by a reporter shows partiers crowded together in a pool at the Lake of the Ozarks, Missouri this Memorial Day weekend.\r\nScott Pasmore, an anchor for CNN affiliate KTVK, shot the vid… [+2140 chars]"
    },
    {
        "uniqueId": 7,
        "source": {
            "id": null,
            "name": "Cnbc.com"
        },
        "author": "Jasmine Kim",
        "title": "China will likely face U.S. sanctions over Hong Kong national security law, White House says - CNBC",
        "description": "The U.S. government could impose sanctions against China, National Security Advisor Robert O'Brien said on Sunday in response to China's announcement of new national security laws in Hong Kong that would curtail the city's autonomy and democracy.",
        "url": "https://www.cnbc.com/2020/05/24/white-house-us-likely-impose-sanctions-against-china-over-hong-kong-law.html",
        "urlToImage": "https://image.cnbcfm.com/api/v1/image/106550737-1590339083895gettyimages-1226354073.jpeg?v=1590339109",
        "publishedAt": "2020-05-24T16:55:14Z",
        "content": "The U.S. government will likely impose sanctions on China if Beijing implements national security laws that would give it greater control over autonomous Hong Kong, White House National Security Advi… [+2759 chars]"
    },
    {
        "uniqueId": 8,
        "source": {
            "id": null,
            "name": "Yahoo.com"
        },
            "author": "Tim O'Donnell",
            "title": "Declining infection rate provides challenge for Oxford coronavirus vaccine - Yahoo News",
            "description": "Just days ago, scientists leading the University of Oxford's coronavirus vaccine development expressed optimism about their progress — more than 1,000 pe...",
            "url": "https://news.yahoo.com/declining-infection-rate-provides-challenge-143700638.html",
            "urlToImage": "https://s.yimg.com/ny/api/res/1.2/.1MU_3jqYFfZ3_8IW4VfkA--/YXBwaWQ9aGlnaGxhbmRlcjt3PTEyODA7aD04NTMuMzMzMzMzMzMzMzMzNA--/https://s.yimg.com/uu/api/res/1.2/SWkWTsWxfdHQxZhXSaMVxg--~B/aD01NjA7dz04NDA7c209MTthcHBpZD15dGFjaHlvbg--/https://media.zenfs.com/en/the_week_574/7329a11af69f5c91cc1d0065f0cc115a",
            "publishedAt": "2020-05-24T16:47:44Z",
            "content": "Just days ago, scientists leading the University of Oxford's coronavirus vaccine development expressed optimism about their progress more than 1,000 people in the United Kingdom have been inoculated … [+1246 chars]"
    },
]